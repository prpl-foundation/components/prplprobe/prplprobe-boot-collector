/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS AS IS AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <cloudevent.h>
#include <deviceinfo.h>

#define ME "cloudevent"

static void set_e_timestamp(Prplprobe__Internal__V1__Event *event) {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    event->timestamp = calloc(1, sizeof(Prplprobe__Internal__V1__Timestamp));
    prplprobe__internal__v1__timestamp__init(event->timestamp);
    event->timestamp->seconds = tv.tv_sec;
    event->timestamp->nanos = tv.tv_usec * 1000;
}

void init_event(Prplprobe__Internal__V1__Event *event, const char *kpiname, const char *kpitype) {
    event->kpiname = strdup(kpiname);
    event->kpitype = strdup(kpitype);
    event->event_data = calloc(1, sizeof(Prplprobe__Internal__V1__Any));
    set_e_timestamp(event);
}

void del_event(Prplprobe__Internal__V1__Event *event) {
    free(event->kpiname);
    free(event->kpitype);
    free(event->event_data->type_url);
    free(event->event_data->value.data);
    free(event->event_data);
    free(event->timestamp);
}

static void ce_attval_set_string(Io__Cloudevents__V1__CloudEvent *ce, const char *attr_name, const char *attr_value) {
    for (size_t i = 0; i < ce->n_attributes; i++) {
        if (!strcmp(ce->attributes[i]->key, "")) {
            ce->attributes[i]->key = strdup(attr_name);
            ce->attributes[i]->value = calloc(1, sizeof(Io__Cloudevents__V1__CloudEvent__CloudEventAttributeValue));
            io__cloudevents__v1__cloud_event__cloud_event_attribute_value__init(ce->attributes[i]->value);
            ce->attributes[i]->value->attr_case = IO__CLOUDEVENTS__V1__CLOUD_EVENT__CLOUD_EVENT_ATTRIBUTE_VALUE__ATTR_CE_STRING;
            if (attr_value) {
                ce->attributes[i]->value->ce_string = strdup(attr_value);
            }
            else {
                ce->attributes[i]->value->ce_string = strdup("");
            }
            break;
        }
    }
}

static void ce_attval_set_bool(Io__Cloudevents__V1__CloudEvent *ce, const char *attr_name, bool attr_value) {
    for (size_t i = 0; i < ce->n_attributes; i++) {
        if (!strcmp(ce->attributes[i]->key, "")) {
            ce->attributes[i]->key = strdup(attr_name);
            ce->attributes[i]->value = calloc(1, sizeof(Io__Cloudevents__V1__CloudEvent__CloudEventAttributeValue));
            io__cloudevents__v1__cloud_event__cloud_event_attribute_value__init(ce->attributes[i]->value);
            ce->attributes[i]->value->attr_case = IO__CLOUDEVENTS__V1__CLOUD_EVENT__CLOUD_EVENT_ATTRIBUTE_VALUE__ATTR_CE_BOOLEAN;
            ce->attributes[i]->value->ce_boolean = attr_value;
            break;
        }
    }
}

static void ce_attval_set_int(Io__Cloudevents__V1__CloudEvent *ce, const char *attr_name, int attr_value) {
    for (size_t i = 0; i < ce->n_attributes; i++) {
        if (!strcmp(ce->attributes[i]->key, "")) {
            ce->attributes[i]->key = strdup(attr_name);
            ce->attributes[i]->value = calloc(1, sizeof(Io__Cloudevents__V1__CloudEvent__CloudEventAttributeValue));
            io__cloudevents__v1__cloud_event__cloud_event_attribute_value__init(ce->attributes[i]->value);
            ce->attributes[i]->value->attr_case = IO__CLOUDEVENTS__V1__CLOUD_EVENT__CLOUD_EVENT_ATTRIBUTE_VALUE__ATTR_CE_INTEGER;
            ce->attributes[i]->value->ce_integer = attr_value;
            break;
        }
    }
}

static void ce_attval_set_timestamp(Io__Cloudevents__V1__CloudEvent *ce, const char *attr_name, Prplprobe__Internal__V1__Timestamp *attr_value) {
    for (size_t i = 0; i < ce->n_attributes; i++) {
        if (!strcmp(ce->attributes[i]->key, "")) {
            ce->attributes[i]->key = strdup(attr_name);
            ce->attributes[i]->value = calloc(1, sizeof(Io__Cloudevents__V1__CloudEvent__CloudEventAttributeValue));
            io__cloudevents__v1__cloud_event__cloud_event_attribute_value__init(ce->attributes[i]->value);
            ce->attributes[i]->value->attr_case = IO__CLOUDEVENTS__V1__CLOUD_EVENT__CLOUD_EVENT_ATTRIBUTE_VALUE__ATTR_CE_TIMESTAMP;

            ce->attributes[i]->value->ce_timestamp = calloc(1, sizeof(Prplprobe__Internal__V1__Timestamp));
            prplprobe__internal__v1__timestamp__init(ce->attributes[i]->value->ce_timestamp);
            ce->attributes[i]->value->ce_timestamp->seconds = attr_value->seconds;
            ce->attributes[i]->value->ce_timestamp->nanos = attr_value->nanos;
            break;
        }
    }
}


void add_event_to_batch(Io__Cloudevents__V1__CloudEventBatch *ce_batch, Prplprobe__Internal__V1__Event event) {

    // new cloud event and copy event
    Io__Cloudevents__V1__CloudEvent *ce = calloc(1, sizeof(Io__Cloudevents__V1__CloudEvent));
    io__cloudevents__v1__cloud_event__init(ce);

    ce->id = strdup("1234-1234-1234");
    ce->spec_version = strdup("1.0");
    char *type = NULL;
    if (asprintf(&type, "%s.%s", event.kpiname, event.kpitype) < 0) {
        SAH_TRACEZ_ERROR(ME, "No memory available");
    }
    ce->type = type;
    ce->source = strdup(event.kpiname);

    ce->n_attributes = 17;
    ce->attributes = calloc(ce->n_attributes, sizeof(Io__Cloudevents__V1__CloudEvent__AttributesEntry *));
    for (size_t i = 0; i < ce->n_attributes; i++) {
        ce->attributes[i] = calloc(1, sizeof(Io__Cloudevents__V1__CloudEvent__AttributesEntry));
        io__cloudevents__v1__cloud_event__attributes_entry__init(ce->attributes[i]);
    }

    ce_attval_set_timestamp(ce, "time", event.timestamp);
    char *dataschema = NULL;
    if (asprintf(&dataschema, "urn:probe:%s_%s", event.kpiname, event.kpitype) < 0) {
        SAH_TRACEZ_ERROR(ME, "No memory available");
    }
    toLower(dataschema);
    ce_attval_set_string(ce, "dataschema", dataschema);
    free(dataschema);
    ce_attval_set_string(ce, "header_version", "1.0");
    ce_attval_set_string(ce, "datacontenttype", "application/protobuf");
    ce_attval_set_string(ce, "kpiname", event.kpiname);
    ce_attval_set_string(ce, "kpitype", event.kpitype);
    ce_attval_set_string(ce, "deviceinfo_type", get_deviceinfo()->device_type);
    ce_attval_set_string(ce, "deviceinfo_cpetype", get_deviceinfo()->cpe_type);
    ce_attval_set_string(ce, "deviceinfo_serialnum", get_deviceinfo()->serial_number);
    ce_attval_set_string(ce, "deviceinfo_macaddress", get_deviceinfo()->mac_address);
    ce_attval_set_string(ce, "deviceinfo_firmware", get_deviceinfo()->firmware);
    ce_attval_set_string(ce, "deviceinfo_ipaddress", get_deviceinfo()->ip_address);
    ce_attval_set_string(ce, "deviceinfo_timezone", get_timezone());
    ce_attval_set_string(ce, "deviceinfo_repo", "figaro");
    ce_attval_set_bool(ce, "configuration_wifi_enable", false);
    ce_attval_set_int(ce, "configuration_wifi_id", -1);
    ce_attval_set_int(ce, "configuration_wifi_version", 0);

    ce->data_case = IO__CLOUDEVENTS__V1__CLOUD_EVENT__DATA_PROTO_DATA;
    ce->proto_data = calloc(1, sizeof(Prplprobe__Internal__V1__Any));
    prplprobe__internal__v1__any__init(ce->proto_data);
    ce->proto_data->type_url = strdup(event.event_data->type_url);
    ce->proto_data->value.len = event.event_data->value.len;
    ce->proto_data->value.data = malloc(event.event_data->value.len);
    memcpy(ce->proto_data->value.data, event.event_data->value.data, event.event_data->value.len);

    // Add ce to the ce batch
    for (size_t i = 0; i < ce_batch->n_events; i++) {
        if (ce_batch->events[i] == NULL) {
            ce_batch->events[i] = ce;
            break;
        }
    }
}

void init_ce_batch(Io__Cloudevents__V1__CloudEventBatch *ce_batch, size_t size) {
    ce_batch->n_events = size;
    ce_batch->events = (Io__Cloudevents__V1__CloudEvent **) calloc(size, sizeof(Io__Cloudevents__V1__CloudEvent *));
    for (size_t i = 0; i < size; i++) {
        ce_batch->events[i] = NULL;
        // ce_batch->events[i] = calloc(1, sizeof(Io__Cloudevents__V1__CloudEvent));
        // io__cloudevents__v1__cloud_event__init(ce_batch->events[i]);
    }
}

void del_ce_batch(Io__Cloudevents__V1__CloudEventBatch *ce_batch) {
    for (size_t i = 0; i < ce_batch->n_events; i++) {
        if (strcmp(ce_batch->events[i]->id, "")) {
            free(ce_batch->events[i]->id);
            free(ce_batch->events[i]->spec_version);
            free(ce_batch->events[i]->type);
            free(ce_batch->events[i]->source);
            for (size_t j = 0; j < ce_batch->events[i]->n_attributes; j++) {
                free(ce_batch->events[i]->attributes[j]->key);
                if (ce_batch->events[i]->attributes[j]->value->attr_case == IO__CLOUDEVENTS__V1__CLOUD_EVENT__CLOUD_EVENT_ATTRIBUTE_VALUE__ATTR_CE_STRING) {
                    free(ce_batch->events[i]->attributes[j]->value->ce_string);
                }
                else if (ce_batch->events[i]->attributes[j]->value->attr_case == IO__CLOUDEVENTS__V1__CLOUD_EVENT__CLOUD_EVENT_ATTRIBUTE_VALUE__ATTR_CE_TIMESTAMP) {
                    free(ce_batch->events[i]->attributes[j]->value->ce_timestamp);
                }
                free(ce_batch->events[i]->attributes[j]->value);
                free(ce_batch->events[i]->attributes[j]);
            }
            free(ce_batch->events[i]->attributes);
            free(ce_batch->events[i]->proto_data->type_url);
            free(ce_batch->events[i]->proto_data->value.data);
            free(ce_batch->events[i]->proto_data);
        }
        free(ce_batch->events[i]);
    }
    free(ce_batch->events);
}

/* generate base64 encoded protobuf message from a ce_batch */
char *gen_message(Io__Cloudevents__V1__CloudEventBatch *ce_batch) {
    if (ce_batch->n_events == 0) {
        return NULL;
    }
    size_t ce_batch_len = io__cloudevents__v1__cloud_event_batch__get_packed_size(ce_batch);
    uint8_t *ce_batch_buf = (uint8_t *) malloc(ce_batch_len);
    io__cloudevents__v1__cloud_event_batch__pack(ce_batch, ce_batch_buf);

    char *buf_encoded = b64_encode(ce_batch_buf, ce_batch_len);
    free(ce_batch_buf);
    return buf_encoded;
}
