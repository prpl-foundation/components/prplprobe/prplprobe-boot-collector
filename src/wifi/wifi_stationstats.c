/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS AS IS AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <wifi.h>

#define ME "wifi"

static void send_StationStats(amxc_var_t *data) {
    SAH_TRACEZ_INFO(ME, "Sending wifi StationStats data");
    // ---------------- format data
    // KPI to recover
    Kpi__Wifi__V1__WifiStationStats kpi = KPI__WIFI__V1__WIFI_STATION_STATS__INIT;

    // add KPI to protobuf data type
    kpi.active = GET_BOOL(data, "active");
    kpi.interface = (char *) GET_CHAR(data, "interface");
    kpi.agginterface = (char *) GET_CHAR(data, "agginterface");
    kpi.flag = (char *) GET_CHAR(data, "flag");
    kpi.vapmode = (char *) GET_CHAR(data, "vapmode");
    kpi.hotspottype = (char *) GET_CHAR(data, "hotspottype");
    kpi.macaddress = (char *) GET_CHAR(data, "macaddress");
    kpi.authenticationstate = GET_INT32(data, "authenticationstate");
    kpi.devicepriority = GET_INT32(data, "devicepriority");
    kpi.devicetype = (char *) GET_CHAR(data, "devicetype");
    kpi.downlinkis40mhz = GET_UINT32(data, "downlinkis40mhz");
    kpi.downlinkmcs = GET_UINT32(data, "downlinkmcs");
    kpi.downlinkshortguard = GET_UINT32(data, "downlinkshortguard");
    kpi.inactive = GET_UINT64(data, "inactive");
    kpi.lastdatadownlinkrate = GET_UINT32(data, "lastdatadownlinkrate");
    kpi.lastdatauplinkrate = GET_UINT32(data, "lastdatauplinkrate");
    kpi.mode = (char *) GET_CHAR(data, "mode");
    kpi.noise = GET_INT32(data, "noise");

    kpi.rxretrans = GET_UINT64(data, "rxretrans");
    kpi.txretrans = GET_UINT64(data, "txretrans");

    kpi.rxbytes = GET_UINT64(data, "rxbytes");
    kpi.txbytes = GET_UINT64(data, "txbytes");
    kpi.rxpacketcount = GET_UINT64(data, "rxpacketcount");
    kpi.txpacketcount = GET_UINT64(data, "txpacketcount");
    kpi.signalnoiseratio = GET_UINT32(data, "signalnoiseratio");
    kpi.uplinkis40mhz = GET_UINT32(data, "uplinkis40mhz");
    kpi.uplinkmcs = GET_UINT32(data, "uplinkmcs");
    kpi.uplinkshortguard = GET_UINT32(data, "uplinkshortguard");
    kpi.maxbandwidthsupported = (char *) GET_CHAR(data, "maxbandwidthsupported");
    kpi.maxdownlinkratereached = GET_UINT32(data, "maxdownlinkratereached");
    kpi.maxdownlinkratesupported = GET_UINT32(data, "maxdownlinkratesupported");
    kpi.maxrxspatialstreamssupported = GET_UINT32(data, "maxrxspatialstreamssupported");
    kpi.signalstrength = GET_INT32(data, "signalstrength");
    kpi.maxsignalstrength = GET_INT32(data, "maxsignalstrength");
    kpi.averagesignalstrength = GET_INT32(data, "averagesignalstrength");

    amxc_llist_t *csv_list = amxc_var_dyncast(amxc_llist_t, GET_ARG(data, "signalstrengthbychain"));
    kpi.signalstrengthbychain = (double *) calloc(amxc_llist_size(csv_list), sizeof(double));
    kpi.n_signalstrengthbychain = 0;

    amxc_llist_for_each(it, csv_list) {
        kpi.signalstrengthbychain[kpi.n_signalstrengthbychain++] = amxc_var_dyncast(double, amxc_var_from_llist_it(it));
    }
    amxc_llist_delete(&csv_list, variant_list_it_free);

    // ---------------- encapsulate and send ----------------
    const char *kpi_name = "Wifi";
    const char *kpi_type = "StationStats";
    char kpi_url[100];
    snprintf(kpi_url, sizeof(kpi_url), "%s%s", TYPE_URL, kpi_type);

    // Init the event
    Prplprobe__Internal__V1__Event e_probe = PRPLPROBE__INTERNAL__V1__EVENT__INIT;
    init_event(&e_probe, kpi_name, kpi_type);

    // Macro to set data to any type of event
    set_event_data(kpi__wifi__v1__wifi_station_stats, e_probe, kpi, kpi_url);

    // ---------------- send ----------------

    // Cloud event
    Io__Cloudevents__V1__CloudEventBatch ce_batch = IO__CLOUDEVENTS__V1__CLOUD_EVENT_BATCH__INIT;
    init_ce_batch(&ce_batch, 1);

    // Add event to ce_batch
    add_event_to_batch(&ce_batch, e_probe);

    // generate and send the event (base64 encoded serialized protobuf message)
    char *b64_msg = gen_message(&ce_batch);
    if (b64_msg) {
        dc_logEvent(b64_msg, strlen(b64_msg) + 1, kpi_name);
        free(b64_msg);
    }

    // free all resources
    del_ce_batch(&ce_batch);
    del_event(&e_probe);

    // ---------------- Clean data struct ----------------
    free(kpi.signalstrengthbychain);
}

void wifi_StationStats(void) {
    amxb_bus_ctx_t *bus_ctx = get_bus_ctx();
    if (!bus_ctx) {
        SAH_TRACEZ_ERROR(ME, "Cannot retrieve bus ctx");
        return;
    }

    int retval = 0;

    amxc_var_t ret_instances;
    amxc_var_init(&ret_instances);
    retval = amxb_call(bus_ctx, "WiFi.AccessPoint.", "_get_instances", NULL, &ret_instances, 5);
    if (retval != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to get %s , Return status = %d", "WiFi.AccessPoint.", retval);
    }

    amxc_var_for_each(it_var, amxc_var_get_path(&ret_instances, "0", AMXC_VAR_FLAG_DEFAULT)) {
        const char *radio_key = amxc_var_key(it_var);

        char path[128];
        amxc_var_t ret;
        amxc_var_init(&ret);
        snprintf(path, sizeof(path), "%sRadioReference", radio_key);
        retval = amxb_get(bus_ctx, path, 0, &ret, 5);
        if (retval != amxd_status_ok) {
            SAH_TRACEZ_ERROR(ME, "Failed to get %s , Return status = %d", path, retval);
        }
        snprintf(path, sizeof(path), "%s.OperatingFrequencyBand", GETP_CHAR(&ret, "0.0.RadioReference"));
        retval = amxb_get(bus_ctx, path, 0, &ret, 5);
        if (retval != amxd_status_ok) {
            SAH_TRACEZ_ERROR(ME, "Failed to get %s , Return status = %d", path, retval);
        }
        const char *interface = getRadioWifiName(GETP_CHAR(&ret, "0.0.OperatingFrequencyBand"));

        amxc_var_t ret_instances_get;
        amxc_var_init(&ret_instances_get);
        retval = amxb_call(bus_ctx, radio_key, "getStationStats", NULL, &ret_instances_get, 5);
        if (retval != amxd_status_ok) {
            SAH_TRACEZ_ERROR(ME, "Failed to get %s getStationStats, Return status = %d", radio_key, retval);
        }

        amxc_var_for_each(it_var_get, amxc_var_get_path(&ret_instances_get, "0", AMXC_VAR_FLAG_DEFAULT)) {
            amxc_var_t station_stats;
            amxc_var_init(&station_stats);
            amxc_var_set_type(&station_stats, AMXC_VAR_ID_HTABLE);

            amxc_var_add_key(cstring_t, &station_stats, "flag", "VAP");

            amxc_var_add_key(uint32_t, &station_stats, "active", GETP_BOOL(it_var_get, "Active")); // TODO bool type
            amxc_var_add_key(cstring_t, &station_stats, "interface", interface);
            amxc_var_add_key(cstring_t, &station_stats, "agginterface", "");                       // TODO not FOUND
            amxc_var_add_key(cstring_t, &station_stats, "vapmode", "Undefined");
            amxc_var_add_key(cstring_t, &station_stats, "hotspottype", "Undefined");
            amxc_var_add_key(cstring_t, &station_stats, "macaddress", GETP_CHAR(it_var_get, "MACAddress"));
            amxc_var_add_key(int32_t, &station_stats, "authenticationstate", GETP_INT32(it_var_get, "AuthenticationState")); // TODO bool type
            amxc_var_add_key(int32_t, &station_stats, "devicepriority", GETP_INT32(it_var_get, "DevicePriority"));           // TODO Uint32 type
            amxc_var_add_key(cstring_t, &station_stats, "devicetype", GETP_CHAR(it_var_get, "DeviceType"));
            amxc_var_add_key(uint32_t, &station_stats, "downlinkis40mhz", 0);                                                // TODO not FOUND
            amxc_var_add_key(uint32_t, &station_stats, "downlinkmcs", GETP_UINT32(it_var_get, "DownlinkMCS"));
            amxc_var_add_key(uint32_t, &station_stats, "downlinkshortguard", GETP_UINT32(it_var_get, "DownlinkShortGuard"));
            amxc_var_add_key(uint64_t, &station_stats, "inactive", GETP_UINT64(it_var_get, "Inactive")); // TODO uint32 type
            amxc_var_add_key(uint32_t, &station_stats, "lastdatadownlinkrate", GETP_UINT32(it_var_get, "LastDataDownlinkRate"));
            amxc_var_add_key(uint32_t, &station_stats, "lastdatauplinkrate", GETP_UINT32(it_var_get, "LastDataUplinkRate"));
            amxc_var_add_key(cstring_t, &station_stats, "mode", ""); // TODO not FOUND
            amxc_var_add_key(int32_t, &station_stats, "noise", GETP_INT32(it_var_get, "Noise"));

            amxc_var_add_key(uint64_t, &station_stats, "rxretrans", GETP_UINT64(it_var_get, "Rx_Retransmissions")); // TODO uint32 type
            amxc_var_add_key(uint64_t, &station_stats, "txretrans", GETP_UINT64(it_var_get, "Tx_Retransmissions")); // TODO uint32 type

            amxc_var_add_key(uint64_t, &station_stats, "rxbytes", GETP_UINT64(it_var_get, "RxBytes"));
            amxc_var_add_key(uint64_t, &station_stats, "txbytes", GETP_UINT64(it_var_get, "TxBytes"));
            amxc_var_add_key(uint64_t, &station_stats, "rxpacketcount", GETP_UINT64(it_var_get, "RxPacketCount"));       // TODO uint32 type
            amxc_var_add_key(uint64_t, &station_stats, "txpacketcount", GETP_UINT64(it_var_get, "TxPacketCount"));       // TODO uint32 type
            amxc_var_add_key(uint32_t, &station_stats, "signalnoiseratio", GETP_UINT32(it_var_get, "SignalNoiseRatio")); // TODO Int32 type !!!!
            amxc_var_add_key(uint32_t, &station_stats, "uplinkis40mhz", 0);                                              // TODO not FOUND
            amxc_var_add_key(uint32_t, &station_stats, "uplinkmcs", GETP_UINT32(it_var_get, "UplinkMCS"));
            amxc_var_add_key(uint32_t, &station_stats, "uplinkshortguard", GETP_UINT32(it_var_get, "UplinkShortGuard")); // TODO bool type
            amxc_var_add_key(cstring_t, &station_stats, "maxbandwidthsupported", GETP_CHAR(it_var_get, "MaxBandwidthSupported"));
            amxc_var_add_key(uint32_t, &station_stats, "maxdownlinkratereached", GETP_UINT32(it_var_get, "MaxDownlinkRateReached"));
            amxc_var_add_key(uint32_t, &station_stats, "maxdownlinkratesupported", GETP_UINT32(it_var_get, "MaxDownlinkRateSupported"));
            amxc_var_add_key(uint32_t, &station_stats, "maxrxspatialstreamssupported", GETP_UINT32(it_var_get, "MaxRxSpatialStreamsSupported")); // TODO uint16 type
            amxc_var_add_key(int32_t, &station_stats, "signalstrength", GETP_INT32(it_var_get, "SignalStrength"));
            amxc_var_add_key(int32_t, &station_stats, "maxsignalstrength", 0);                                                                   // TODO not FOUND
            amxc_var_add_key(int32_t, &station_stats, "averagesignalstrength", GETP_INT32(it_var_get, "AvgSignalStrength"));
            amxc_var_add_key(cstring_t, &station_stats, "signalstrengthbychain", GETP_CHAR(it_var_get, "SignalStrengthByChain"));

            // amxc_var_dump(&station_stats, STDOUT_FILENO);

            send_StationStats(&station_stats);

            amxc_var_clean(&station_stats);
        }
        amxc_var_clean(&ret_instances_get);
        amxc_var_clean(&ret);
    }
    amxc_var_clean(&ret_instances);
}