/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS AS IS AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <wifi.h>
#define ME "wifi"

static void send_Radio(amxc_var_t *data) {
    SAH_TRACEZ_INFO(ME, "Sending wifi Radio data");
    // ---------------- format data ----------------
    // KPI to recover
    Kpi__Wifi__V1__WifiRadio kpi = KPI__WIFI__V1__WIFI_RADIO__INIT;

    // add KPI to protobuf data type
    kpi.name = (char *) GET_CHAR(data, "name");

    amxc_llist_t *csv_list = amxc_var_dyncast(amxc_llist_t, GET_ARG(data, "cleareddfschannels"));
    kpi.cleareddfschannels = (uint32_t *) calloc(amxc_llist_size(csv_list), sizeof(uint32_t));
    kpi.n_cleareddfschannels = 0;

    amxc_llist_for_each(it, csv_list) {
        kpi.cleareddfschannels[kpi.n_cleareddfschannels++] = amxc_var_dyncast(uint32_t, amxc_var_from_llist_it(it));
    }
    amxc_llist_delete(&csv_list, variant_list_it_free);

    kpi.channel = GET_UINT32(data, "channel");
    kpi.dfschannelchangeeventcounter = GET_UINT32(data, "dfschannelchangeeventcounter");
    kpi.radiostatus = (char *) GET_CHAR(data, "radiostatus");
    kpi.bandwidth = (char *) GET_CHAR(data, "bandwidth");
    kpi.currentbandwidth = (char *) GET_CHAR(data, "currentbandwidth");
    kpi.autochannelenable = (char *) GET_CHAR(data, "autochannelenable");
    kpi.channelchangereason = (char *) GET_CHAR(data, "channelchangereason");
    kpi.frequency = (char *) GET_CHAR(data, "frequency");
    kpi.status = (char *) GET_CHAR(data, "status");

    // ---------------- encapsulate and send ----------------
    const char *kpi_name = "Wifi";
    const char *kpi_type = "Radio";
    char kpi_url[100];
    snprintf(kpi_url, sizeof(kpi_url), "%s%s", TYPE_URL, kpi_type);

    // Init the event
    Prplprobe__Internal__V1__Event e_probe = PRPLPROBE__INTERNAL__V1__EVENT__INIT;
    init_event(&e_probe, kpi_name, kpi_type);

    // Macro to set data to any type of event
    set_event_data(kpi__wifi__v1__wifi_radio, e_probe, kpi, kpi_url);

    // ---------------- send ----------------

    // Cloud event
    Io__Cloudevents__V1__CloudEventBatch ce_batch = IO__CLOUDEVENTS__V1__CLOUD_EVENT_BATCH__INIT;
    init_ce_batch(&ce_batch, 1);

    // Add event to ce_batch
    add_event_to_batch(&ce_batch, e_probe);

    // generate and send the event (base64 encoded serialized protobuf message)
    char *b64_msg = gen_message(&ce_batch);
    if (b64_msg) {
        dc_logEvent(b64_msg, strlen(b64_msg) + 1, kpi_name);
        free(b64_msg);
    }

    // free all resources
    del_ce_batch(&ce_batch);
    del_event(&e_probe);

    // ---------------- Clean data struct ----------------
    free(kpi.cleareddfschannels);
}

//TODO Put on EVENT !
// Trigger at boot and event
void wifi_Radio(void) {
    amxb_bus_ctx_t *bus_ctx = get_bus_ctx();
    if (!bus_ctx) {
        SAH_TRACEZ_ERROR(ME, "Cannot retrieve bus ctx");
        return;
    }

    int retval = 0;
    char path[128];

    amxc_var_t ret_instances;
    amxc_var_init(&ret_instances);
    retval = amxb_call(bus_ctx, "WiFi.Radio.", "_get_instances", NULL, &ret_instances, 5);
    if (retval != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to get %s , Return status = %d", "WiFi.Radio.", retval);
    }

    amxc_var_for_each(it_var, amxc_var_get_path(&ret_instances, "0", AMXC_VAR_FLAG_DEFAULT)) {
        const char *radio_key = amxc_var_key(it_var);

        amxc_var_t radio;
        amxc_var_init(&radio);
        amxc_var_set_type(&radio, AMXC_VAR_ID_HTABLE);
        amxc_var_t ret;
        amxc_var_init(&ret);

        snprintf(path, sizeof(path), "%sOperatingFrequencyBand", radio_key);
        retval = amxb_get(bus_ctx, path, 0, &ret, 5);
        if (retval != amxd_status_ok) {
            SAH_TRACEZ_ERROR(ME, "Failed to get %s , Return status = %d", path, retval);
        }
        amxc_var_add_key(cstring_t, &radio, "name", getRadioWifiName(GETP_CHAR(&ret, "0.0.OperatingFrequencyBand")));
        amxc_var_add_key(cstring_t, &radio, "frequency", GETP_CHAR(&ret, "0.0.OperatingFrequencyBand"));

        snprintf(path, sizeof(path), "%sChannelMgt.", radio_key);
        retval = amxb_get(bus_ctx, path, 0, &ret, 5);
        if (retval != amxd_status_ok) {
            SAH_TRACEZ_ERROR(ME, "Failed to get %s , Return status = %d", path, retval);
        }
        amxc_var_add_key(cstring_t, &radio, "cleareddfschannels", GETP_CHAR(&ret, "0.0.ClearedDfsChannels"));
        amxc_var_add_key(cstring_t, &radio, "radiostatus", GETP_CHAR(&ret, "0.0.RadioStatus"));

        snprintf(path, sizeof(path), "%s", radio_key);
        retval = amxb_get(bus_ctx, path, 0, &ret, 5);
        if (retval != amxd_status_ok) {
            SAH_TRACEZ_ERROR(ME, "Failed to get %s , Return status = %d", path, retval);
        }
        amxc_var_add_key(uint32_t, &radio, "channel", GETP_UINT32(&ret, "0.0.Channel"));
        amxc_var_add_key(uint32_t, &radio, "dfschannelchangeeventcounter", GETP_UINT32(&ret, "0.0.DFSChannelChangeEventCounter"));
        amxc_var_add_key(cstring_t, &radio, "currentbandwidth", GETP_CHAR(&ret, "0.0.CurrentOperatingChannelBandwidth"));
        amxc_var_add_key(cstring_t, &radio, "autochannelenable", GETP_BOOL(&ret, "0.0.AutoChannelEnable") ? "TRUE" : "FALSE");
        amxc_var_add_key(cstring_t, &radio, "channelchangereason", GETP_CHAR(&ret, "0.0.ChannelChangeReason"));
        amxc_var_add_key(cstring_t, &radio, "status", GETP_CHAR(&ret, "0.0.Status"));
        amxc_var_add_key(cstring_t, &radio, "bandwidth", GETP_CHAR(&ret, "0.0.OperatingChannelBandwidth"));

        // amxc_var_dump(&radio, STDOUT_FILENO);

        send_Radio(&radio);

        amxc_var_clean(&ret);
        amxc_var_clean(&radio);
    }
    amxc_var_clean(&ret_instances);
}