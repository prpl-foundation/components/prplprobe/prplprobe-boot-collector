/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS AS IS AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <wifi.h>

#define ME "wifi"

static void send_InterfaceStats(amxc_var_t *data) {
    SAH_TRACEZ_INFO(ME, "Sending wifi InterfaceStats data");
    // ---------------- format data ----------------
    // KPI to recover
    Kpi__Wifi__V1__WifiInterfaceStats kpi = KPI__WIFI__V1__WIFI_INTERFACE_STATS__INIT;

    // add KPI to protobuf data type
    kpi.interface = (char *) GET_CHAR(data, "interface");
    kpi.agginterface = (char *) GET_CHAR(data, "agginterface");
    kpi.flag = (char *) GET_CHAR(data, "flag");

    kpi.broadcastpacketsreceived = GET_UINT64(data, "broadcastpacketsreceived");
    kpi.broadcastpacketssent = GET_UINT64(data, "broadcastpacketssent");
    kpi.bytesreceived = GET_UINT64(data, "bytesreceived");
    kpi.bytessent = GET_UINT64(data, "bytessent");
    kpi.discardpacketsreceived = GET_UINT64(data, "discardpacketsreceived");
    kpi.multicastpacketsreceived = GET_UINT64(data, "multicastpacketsreceived");
    kpi.multicastpacketssent = GET_UINT64(data, "multicastpacketssent");
    kpi.packetsreceived = GET_UINT64(data, "packetsreceived");
    kpi.packetssent = GET_UINT64(data, "packetssent");
    kpi.rxerror = GET_UINT64(data, "rxerror");
    kpi.txerror = GET_UINT64(data, "txerror");
    kpi.txnobuf = GET_UINT64(data, "txnobuf");
    kpi.txretrans = GET_UINT64(data, "txretrans");
    kpi.unicastpacketsreceived = GET_UINT64(data, "unicastpacketsreceived");
    kpi.unicastpacketssent = GET_UINT64(data, "unicastpacketssent");
    kpi.unknownprotopacketsreceived = GET_UINT64(data, "unknownprotopacketsreceived");

    int32_t array_wmmfailedsent[] = {GETP_INT32(data, "wmmfailedsent.AC_BE"),
        GETP_INT32(data, "wmmfailedsent.AC_BK"),
        GETP_INT32(data, "wmmfailedsent.AC_VO"),
        GETP_INT32(data, "wmmfailedsent.AC_VI")};
    kpi.n_wmmfailedsent = 4;
    kpi.wmmfailedsent = array_wmmfailedsent; // uint64_t list

    kpi.vapmode = (char *) GET_CHAR(data, "vapmode");
    kpi.hotspottype = (char *) GET_CHAR(data, "hotspottype");

    // ---------------- encapsulate and send ----------------

    const char *kpi_name = "Wifi";
    const char *kpi_type = "InterfaceStats";
    char kpi_url[100];
    snprintf(kpi_url, sizeof(kpi_url), "%s%s", TYPE_URL, kpi_type);

    // Init the event
    Prplprobe__Internal__V1__Event e_probe = PRPLPROBE__INTERNAL__V1__EVENT__INIT;
    init_event(&e_probe, kpi_name, kpi_type);

    // Macro to set data to any type of event
    set_event_data(kpi__wifi__v1__wifi_interface_stats, e_probe, kpi, kpi_url);

    // Cloud event
    Io__Cloudevents__V1__CloudEventBatch ce_batch = IO__CLOUDEVENTS__V1__CLOUD_EVENT_BATCH__INIT;
    init_ce_batch(&ce_batch, 1);

    // Add event to ce_batch
    add_event_to_batch(&ce_batch, e_probe);

    // generate and send the event (base64 encoded serialized protobuf message)
    char *b64_msg = gen_message(&ce_batch);
    if (b64_msg) {
        dc_logEvent(b64_msg, strlen(b64_msg) + 1, kpi_name);
        free(b64_msg);
    }

    // free all resources
    del_ce_batch(&ce_batch);
    del_event(&e_probe);
}

// Trigger periodic
void wifi_InterfaceStats(const char *interface) {
    if (strcmp(interface, "Radio") && strcmp(interface, "SSID")) {
        SAH_TRACEZ_ERROR(ME, "Interface not defined");
        return;
    }

    amxb_bus_ctx_t *bus_ctx = get_bus_ctx();
    if (!bus_ctx) {
        SAH_TRACEZ_ERROR(ME, "Cannot retrieve bus ctx");
        return;
    }

    int retval = 0;

    char path[128];
    snprintf(path, sizeof(path), "WiFi.%s.", interface);

    amxc_var_t ret_instances;
    amxc_var_init(&ret_instances);
    retval = amxb_call(bus_ctx, path, "_get_instances", NULL, &ret_instances, 5);
    if (retval != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to get %s , Return status = %d", path, retval);
    }

    amxc_var_for_each(it_var, amxc_var_get_path(&ret_instances, "0", AMXC_VAR_FLAG_DEFAULT)) {
        const char *radio_key = amxc_var_key(it_var);

        amxc_var_t interface_stats;
        amxc_var_init(&interface_stats);
        amxc_var_set_type(&interface_stats, AMXC_VAR_ID_HTABLE);

        amxc_var_t ret;
        amxc_var_init(&ret);

        amxc_var_add_key(cstring_t, &interface_stats, "flag", strcmp(interface, "SSID") ? "Radio" : "VAP");

        // Get the OperatingFrequencyBand to fill the interface
        if (strcmp(interface, "Radio") == 0) {
            snprintf(path, sizeof(path), "%sOperatingFrequencyBand", radio_key);
        }
        else {
            snprintf(path, sizeof(path), "%sLowerLayers", radio_key);
            retval = amxb_get(bus_ctx, path, 0, &ret, 5);
            if (retval != amxd_status_ok) {
                SAH_TRACEZ_ERROR(ME, "Failed to get %s , Return status = %d", path, retval);
            }
            snprintf(path, sizeof(path), "%s.OperatingFrequencyBand", GETP_CHAR(&ret, "0.0.LowerLayers"));
        }
        retval = amxb_get(bus_ctx, path, 0, &ret, 5);
        if (retval != amxd_status_ok) {
            SAH_TRACEZ_ERROR(ME, "Failed to get %s , Return status = %d", path, retval);
        }
        amxc_var_add_key(cstring_t, &interface_stats, "interface", getRadioWifiName(GETP_CHAR(&ret, "0.0.OperatingFrequencyBand")));

        snprintf(path, sizeof(path), "%s", radio_key);
        retval = amxb_call(bus_ctx, path, (strcmp(interface, "SSID") ? "getRadioStats" : "getSSIDStats"), NULL, &ret, 5);
        if (retval != amxd_status_ok) {
            SAH_TRACEZ_ERROR(ME, "Failed to get %s get Stats, Return status = %d", path, retval);
        }
        amxc_var_add_key(uint64_t, &interface_stats, "broadcastpacketsreceived", GETP_UINT64(&ret, "0.BroadcastPacketsReceived"));
        amxc_var_add_key(uint64_t, &interface_stats, "broadcastpacketssent", GETP_UINT64(&ret, "0.BroadcastPacketsSent"));
        amxc_var_add_key(uint64_t, &interface_stats, "bytesreceived", GETP_UINT64(&ret, "0.BytesReceived"));
        amxc_var_add_key(uint64_t, &interface_stats, "bytessent", GETP_UINT64(&ret, "0.BytesSent"));
        amxc_var_add_key(uint64_t, &interface_stats, "discardpacketsreceived", GETP_UINT64(&ret, "0.DiscardPacketsReceived"));
        amxc_var_add_key(uint64_t, &interface_stats, "multicastpacketsreceived", GETP_UINT64(&ret, "0.MulticastPacketsReceived"));
        amxc_var_add_key(uint64_t, &interface_stats, "multicastpacketssent", GETP_UINT64(&ret, "0.MulticastPacketsSent"));
        amxc_var_add_key(uint64_t, &interface_stats, "packetsreceived", GETP_UINT64(&ret, "0.PacketsReceived"));
        amxc_var_add_key(uint64_t, &interface_stats, "packetssent", GETP_UINT64(&ret, "0.PacketsSent"));
        amxc_var_add_key(uint64_t, &interface_stats, "rxerror", GETP_UINT64(&ret, "0.ErrorsReceived"));
        amxc_var_add_key(uint64_t, &interface_stats, "txerror", GETP_UINT64(&ret, "0.ErrorsSent"));
        amxc_var_add_key(uint64_t, &interface_stats, "txnobuf", GETP_UINT64(&ret, "0.DiscardPacketsSent"));
        amxc_var_add_key(uint64_t, &interface_stats, "txretrans", GETP_UINT64(&ret, "0.RetransCount"));
        amxc_var_add_key(uint64_t, &interface_stats, "unicastpacketsreceived", GETP_UINT64(&ret, "0.UnicastPacketsReceived"));
        amxc_var_add_key(uint64_t, &interface_stats, "unicastpacketssent", GETP_UINT64(&ret, "0.UnicastPacketsSent"));
        amxc_var_add_key(uint64_t, &interface_stats, "unknownprotopacketsreceived", GETP_UINT64(&ret, "0.UnknownProtoPacketsReceived"));
        amxc_var_add_key(amxc_htable_t, &interface_stats, "wmmfailedsent", amxc_var_constcast(amxc_htable_t, GETP_ARG(&ret, "0.WmmFailedSent")));

        // TODO find values corresponding
        amxc_var_add_key(cstring_t, &interface_stats, "agginterface", "");         // TODO need to be FOUND
        amxc_var_add_key(cstring_t, &interface_stats, "vapmode", "Undefined");     // TODO need to be FOUND
        amxc_var_add_key(cstring_t, &interface_stats, "hotspottype", "Undefined"); // TODO need to be FOUND

        // amxc_var_dump(&interface_stats, STDOUT_FILENO);

        send_InterfaceStats(&interface_stats);

        amxc_var_clean(&ret);
        amxc_var_clean(&interface_stats);
    }
    amxc_var_clean(&ret_instances);
}