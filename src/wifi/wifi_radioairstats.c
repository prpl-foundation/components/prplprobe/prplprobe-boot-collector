/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS AS IS AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <wifi.h>

#define ME "wifi"

static void send_RadioAirStats(amxc_var_t *data) {
    SAH_TRACEZ_INFO(ME, "Sending wifi RadioAirStats data");
    // ---------------- format data ----------------
    // KPI to recover
    Kpi__Wifi__V1__WifiRadioAirStats kpi = KPI__WIFI__V1__WIFI_RADIO_AIR_STATS__INIT;

    // add KPI to protobuf data type
    kpi.interface = (char *) GET_CHAR(data, "interface");
    kpi.channel = GET_UINT32(data, "channel");
    kpi.txop = GET_UINT32(data, "txop");
    kpi.load = GET_UINT32(data, "load");
    kpi.txtime = GET_UINT32(data, "txtime");
    kpi.rxtime = GET_UINT32(data, "rxtime");
    kpi.inttime = GET_UINT32(data, "inttime");
    kpi.freetime = GET_UINT32(data, "freetime");
    kpi.totaltime = GET_UINT32(data, "totaltime");
    kpi.noise = GET_UINT32(data, "noise");

    uint32_t array_shortpreambleerror[] = {GET_UINT32(data, "shortpreambleerror")};
    kpi.n_shortpreambleerror = 1; // uint32_t
    kpi.shortpreambleerror = array_shortpreambleerror;

    uint32_t array_longpreambleerror[] = {GET_UINT32(data, "longpreambleerror")};
    kpi.n_longpreambleerror = 1;
    kpi.longpreambleerror = array_longpreambleerror; // uint32_t list

    uint32_t array_glitch[] = {GET_UINT32(data, "glitch")};
    kpi.n_glitch = 1;
    kpi.glitch = array_glitch; // int32_t list

    uint32_t array_badplcp[] = {GET_UINT32(data, "badplcp")};
    kpi.n_badplcp = 1;
    kpi.badplcp = array_badplcp; // int32_t list

    // ---------------- encapsulate and send ----------------
    const char *kpi_name = "Wifi";
    const char *kpi_type = "RadioAirStats";
    char kpi_url[100];
    snprintf(kpi_url, sizeof(kpi_url), "%s%s", TYPE_URL, kpi_type);

    // Init the event
    Prplprobe__Internal__V1__Event e_probe = PRPLPROBE__INTERNAL__V1__EVENT__INIT;
    init_event(&e_probe, kpi_name, kpi_type);

    // Macro to set data to any type of event
    set_event_data(kpi__wifi__v1__wifi_radio_air_stats, e_probe, kpi, kpi_url);

    // ---------------- send ----------------

    // Cloud event
    Io__Cloudevents__V1__CloudEventBatch ce_batch = IO__CLOUDEVENTS__V1__CLOUD_EVENT_BATCH__INIT;
    init_ce_batch(&ce_batch, 1);

    // Add event to ce_batch
    add_event_to_batch(&ce_batch, e_probe);

    // generate and send the event (base64 encoded serialized protobuf message)
    char *b64_msg = gen_message(&ce_batch);
    if (b64_msg) {
        dc_logEvent(b64_msg, strlen(b64_msg) + 1, kpi_name);
        free(b64_msg);
    }

    // free all resources
    del_ce_batch(&ce_batch);
    del_event(&e_probe);
}

void wifi_RadioAirStats(void) {
    amxb_bus_ctx_t *bus_ctx = get_bus_ctx();
    if (!bus_ctx) {
        SAH_TRACEZ_ERROR(ME, "Cannot retrieve bus ctx");
        return;
    }

    int retval = 0;
    char path[128];

    amxc_var_t ret_instances;
    amxc_var_init(&ret_instances);
    retval = amxb_call(bus_ctx, "WiFi.Radio.", "_get_instances", NULL, &ret_instances, 5);
    if (retval != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to get %s , Return status = %d", "WiFi.Radio.", retval);
    }

    amxc_var_for_each(it_var, amxc_var_get_path(&ret_instances, "0", AMXC_VAR_FLAG_DEFAULT)) {
        const char *radio_key = amxc_var_key(it_var);

        amxc_var_t radio_air_stats;
        amxc_var_init(&radio_air_stats);
        amxc_var_set_type(&radio_air_stats, AMXC_VAR_ID_HTABLE);
        amxc_var_t ret;
        amxc_var_init(&ret);

        snprintf(path, sizeof(path), "%s", radio_key);
        retval = amxb_get(bus_ctx, path, 0, &ret, 5);
        if (retval != amxd_status_ok) {
            SAH_TRACEZ_ERROR(ME, "Failed to get %s , Return status = %d", path, retval);
        }
        amxc_var_add_key(cstring_t, &radio_air_stats, "interface", getRadioWifiName(GETP_CHAR(&ret, "0.0.OperatingFrequencyBand")));
        amxc_var_add_key(uint32_t, &radio_air_stats, "channel", GETP_UINT32(&ret, "0.0.Channel"));

        /* TotalTime = (IntTime) + FreeTime + TxTime +RxTime &  IntTime = ObssTime + NoiseTime */
        snprintf(path, sizeof(path), "%s", radio_key);
        retval = amxb_call(bus_ctx, path, "getRadioAirStats", NULL, &ret, 5);
        if (retval != amxd_status_ok) {
            SAH_TRACEZ_ERROR(ME, "Failed to get %s getRadioAirStats, Return status = %d", path, retval);
        }
        amxc_var_add_key(uint32_t, &radio_air_stats, "txop", 100 - GETP_UINT32(&ret, "0.Load"));
        amxc_var_add_key(uint32_t, &radio_air_stats, "load", GETP_UINT32(&ret, "0.Load"));
        amxc_var_add_key(uint32_t, &radio_air_stats, "txtime", GETP_UINT32(&ret, "0.TxTime"));
        amxc_var_add_key(uint32_t, &radio_air_stats, "rxtime", GETP_UINT32(&ret, "0.RxTime"));
        amxc_var_add_key(uint32_t, &radio_air_stats, "inttime", GETP_UINT32(&ret, "0.IntTime"));
        amxc_var_add_key(uint32_t, &radio_air_stats, "freetime", GETP_UINT32(&ret, "0.FreeTime"));
        amxc_var_add_key(uint32_t, &radio_air_stats, "totaltime", GETP_UINT32(&ret, "0.TotalTime"));
        amxc_var_add_key(uint32_t, &radio_air_stats, "noise", GETP_UINT32(&ret, "0.Noise")); //TODO negative

        // TODO Name change and values
        amxc_var_add_key(uint32_t, &radio_air_stats, "shortpreambleerror", GETP_UINT32(&ret, "0.ShortPreambleErrorPercentage"));
        amxc_var_add_key(uint32_t, &radio_air_stats, "longpreambleerror", GETP_UINT32(&ret, "0.LongPreambleErrorPercentage"));
        // TODO Array are now int
        amxc_var_add_key(uint32_t, &radio_air_stats, "glitch", GETP_UINT32(&ret, "0.VendorStats.Glitch"));
        amxc_var_add_key(uint32_t, &radio_air_stats, "badplcp", GETP_UINT32(&ret, "0.VendorStats.BadPlcp"));

        // amxc_var_dump(&radio_air_stats, STDOUT_FILENO);

        send_RadioAirStats(&radio_air_stats);

        amxc_var_clean(&ret);
        amxc_var_clean(&radio_air_stats);
    }
    amxc_var_clean(&ret_instances);
}