/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS AS IS AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <wifi.h>

#define ME "wifi"

static void send_ScanCount(amxc_var_t *data) {
    SAH_TRACEZ_INFO(ME, "Sending wifi ScanCount data");
    // ---------------- format data
    // KPI to recover
    Kpi__Wifi__V1__WifiScanCount kpi = KPI__WIFI__V1__WIFI_SCAN_COUNT__INIT;

    // add KPI to protobuf data type
    kpi.interface = (char *) GET_CHAR(data, "interface");
    kpi.ssidcount = GET_UINT32(data, "ssidcount");

    // ---------------- encapsulate and send ----------------
    const char *kpi_name = "Wifi";
    const char *kpi_type = "ScanCount";
    char kpi_url[100];
    snprintf(kpi_url, sizeof(kpi_url), "%s%s", TYPE_URL, kpi_type);

    // Init the event
    Prplprobe__Internal__V1__Event e_probe = PRPLPROBE__INTERNAL__V1__EVENT__INIT;
    init_event(&e_probe, kpi_name, kpi_type);

    // Macro to set data to any type of event
    set_event_data(kpi__wifi__v1__wifi_scan_count, e_probe, kpi, kpi_url);

    // ---------------- send ----------------

    // Cloud event
    Io__Cloudevents__V1__CloudEventBatch ce_batch = IO__CLOUDEVENTS__V1__CLOUD_EVENT_BATCH__INIT;
    init_ce_batch(&ce_batch, 1);

    // Add event to ce_batch
    add_event_to_batch(&ce_batch, e_probe);

    // generate and send the event (base64 encoded serialized protobuf message)
    char *b64_msg = gen_message(&ce_batch);
    if (b64_msg) {
        dc_logEvent(b64_msg, strlen(b64_msg) + 1, kpi_name);
        free(b64_msg);
    }

    // free all resources
    del_ce_batch(&ce_batch);
    del_event(&e_probe);
}

void wifi_ScanCount(void) {
    amxb_bus_ctx_t *bus_ctx = get_bus_ctx();
    if (!bus_ctx) {
        SAH_TRACEZ_ERROR(ME, "Cannot retrieve bus ctx");
        return;
    }

    int retval = 0;

    // !! TODO Redo this part with the scan function of each radio, & asynchronously because of timeout !!
    amxc_var_t ret_instances;
    amxc_var_init(&ret_instances);
    retval = amxb_call(bus_ctx, "WiFi.", "NeighboringWiFiDiagnostic", NULL, &ret_instances, 20);
    if (retval != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to get WiFi NeighboringWiFiDiagnostic, Return status = %d", retval);
        goto leave;
    }

    const char *call_status = GETP_CHAR(&ret_instances, "0.Status");
    if ((call_status == NULL) || strcmp("Complete", call_status)) {
        SAH_TRACEZ_ERROR(ME, "Failed to Complete WiFi NeighboringWiFiDiagnostic, Status = %s", call_status);
        goto leave;
    }

    {
        amxc_var_t scan_count;
        amxc_var_init(&scan_count);
        amxc_var_set_type(&scan_count, AMXC_VAR_ID_HTABLE);

        amxc_var_add_key(cstring_t, &scan_count, "interface", getRadioWifiName(GETP_CHAR(&ret_instances, "0.Result.0.OperatingFrequencyBand")));       // TODO
        amxc_var_add_key(uint32_t, &scan_count, "ssidcount", amxc_llist_size(amxc_var_constcast(amxc_llist_t, GETP_ARG(&ret_instances, "0.Result")))); // TODO

        // amxc_var_dump(&scan_count, STDOUT_FILENO);

        send_ScanCount(&scan_count);

        amxc_var_clean(&scan_count);
    }

leave:
    amxc_var_clean(&ret_instances);
}