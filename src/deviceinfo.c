/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS AS IS AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <deviceinfo.h>

#define ME "deviceinfo"

device_info_t deviceinfo = {NULL};

static void deviceinfo_update(char **field, const char *new_value) {
    if (new_value) {
        *field = strdup(new_value);
        if (!*field) {
            SAH_TRACEZ_ERROR(ME, "Failed to update deviceinfo, not enough memory");
        }
    }
}

void init_deviceinfo(void) {
    const char *deviceinfo_obj = "DeviceInfo.";
    amxb_bus_ctx_t *ctx = get_bus_ctx();
    if (!ctx) {
        SAH_TRACEZ_ERROR(ME, "Cannot retrieve bus ctx");
        return;
    }

    amxc_var_t ret;
    amxc_var_init(&ret);

    amxb_get(ctx, deviceinfo_obj, 1, &ret, 5);
    deviceinfo_update(&deviceinfo.serial_number, GETP_CHAR(&ret, "0.0.SerialNumber"));
    deviceinfo_update(&deviceinfo.firmware, GETP_CHAR(&ret, "0.0.SoftwareVersion"));
    deviceinfo_update(&deviceinfo.cpe_type, GETP_CHAR(&ret, "0.0.Manufacturer"));
    deviceinfo_update(&deviceinfo.device_type, "HGW");

    amxb_get(ctx, "NetModel.Intf.ip-wan.", 0, &ret, 5);
    deviceinfo_update(&deviceinfo.ip_address, GETP_CHAR(&ret, "0.0.IPAddress"));

    amxb_get(ctx, "NetModel.Intf.ethLink-bridge_lan.", 0, &ret, 5);
    deviceinfo_update(&deviceinfo.mac_address, GETP_CHAR(&ret, "0.0.MACAddress"));

    SAH_TRACEZ_INFO(ME, "SerialNumber = %s", deviceinfo.serial_number);
    SAH_TRACEZ_INFO(ME, "SoftwareVersion = %s", deviceinfo.firmware);
    SAH_TRACEZ_INFO(ME, "IPv4Adress = %s", deviceinfo.ip_address);
    SAH_TRACEZ_INFO(ME, "MACAddress = %s", deviceinfo.mac_address);

    amxc_var_clean(&ret);
}

void clean_deviceinfo(void) {
    free(deviceinfo.serial_number);
    deviceinfo.serial_number = NULL;
    free(deviceinfo.firmware);
    deviceinfo.firmware = NULL;
    free(deviceinfo.mac_address);
    deviceinfo.mac_address = NULL;
    free(deviceinfo.ip_address);
    deviceinfo.ip_address = NULL;
    free(deviceinfo.device_type);
    deviceinfo.device_type = NULL;
    free(deviceinfo.cpe_type);
    deviceinfo.cpe_type = NULL;
}

device_info_t *get_deviceinfo(void) {
    return &deviceinfo;
}
