/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS AS IS AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <cpu.h>

#define PROC_DIR "/proc_host/proc/"
#define CPU_PATH PROC_DIR "/stat"
#define STAT_SIZE 5
#define FILE_NAME_SIZE 128
#define LINE_SIZE 1024
#define PATH_DEFAULT_SIZE 250
#define ME "cpu"


long long unsigned int cpuload = 0;
long long unsigned int cpuload_prev = 0;
long long unsigned int cpuload_element[] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
long long unsigned int cpuload_element_prev[] = {0, 0, 0, 0, 0, 0, 0, 0, 0};

struct proc **processes = NULL;
int processes_size = PATH_DEFAULT_SIZE;
int used_size = 0;

struct proc {
    long unsigned int current_user;
    long unsigned int current_system;
    long unsigned int previous_user;
    long unsigned int previous_system;
    long unsigned int ratio_user;
    long unsigned int ratio_system;
    char name[128];
    int32_t pid;
};

/* Common functions */

// Qsort's compare function.
static int compare(const void *a, const void *b) {
    const struct proc *p1 = *(struct proc **) a;
    const struct proc *p2 = *(struct proc **) b;

    // No risk of int overflow as current is always > previous.
    long unsigned ratiop1 = p1->ratio_user + p1->ratio_system;
    long unsigned ratiop2 = p2->ratio_user + p2->ratio_system;

    // 0ull to prevent int overflow
    if (ratiop1 > ratiop2) {
        return -1;
    }
    else if (ratiop1 == ratiop2) {
        return 0;
    }
    else {
        return 1;
    }
}

static int find_process_id_by_pid(struct proc **process, int needle, int size) {
    for (int i = 0; i < size; i++) {
        if (process[i]->pid == needle) {
            return i;
        }
    }
    return -1;
}

static int is_proc(char *name) {
    for (unsigned i = 0; i < strlen(name); i++) {
        if (!isdigit(name[i])) {
            return 0;
        }
    }
    return 1;
}

static void reset_process(struct proc *proc) {
    proc->name[0] = '\0';
    proc->current_user = 0;
    proc->current_system = 0;
    proc->previous_user = 0;
    proc->previous_system = 0;
    proc->ratio_user = 0;
    proc->ratio_system = 0;
    proc->pid = -1;
}

static void shift_processes_array(int from_index) {
    for (int k = from_index; k < used_size; k++) {
        strcpy(processes[k]->name, processes[k + 1]->name);
        processes[k]->current_user = processes[k + 1]->current_user;
        processes[k]->current_system = processes[k + 1]->current_system;
        processes[k]->previous_user = processes[k + 1]->previous_user;
        processes[k]->previous_system = processes[k + 1]->previous_system;
        processes[k]->ratio_user = processes[k + 1]->ratio_user;
        processes[k]->ratio_system = processes[k + 1]->ratio_system;
        processes[k]->pid = processes[k + 1]->pid;
    }
    reset_process(processes[used_size]);
}

static int file_exist(char *filename) {
    struct stat buffer;
    return (stat(filename, &buffer) == 0);
}

static void clean_processes(void) {
    for (int i = 0; i < used_size; i++) {
        int path_len = strlen(PROC_DIR) + 5 + strlen("/status");
        char path[path_len + 1];
        snprintf(path, path_len + 1, PROC_DIR "/%d/status", processes[i]->pid);
        //if the process was killed, we delete it and shift the whole array
        if (!file_exist(path)) {
            used_size -= 1;
            reset_process(processes[i]);
            shift_processes_array(i);
        }
    }
}

static void reset_all_processes(void) {
    for (int i = 0; i < used_size; i++) {
        reset_process(processes[i]);
    }
    used_size = 0;
}

static int check_process_usage(struct proc *process, char *proc_dir) {
    char stat_file_name[strlen(proc_dir) + STAT_SIZE + 1];
    strcpy(stat_file_name, proc_dir);
    strcat(stat_file_name, "/stat");

    FILE *status = fopen(stat_file_name, "r");
    int result = 0;
    if (status != NULL) {
        char line[LINE_SIZE];
        char name[FILE_NAME_SIZE];
        long unsigned int user_time = 0;
        long unsigned int system_time = 0;

        if ((fgets(line, LINE_SIZE, status) != NULL) && (sscanf(line, "%*d %s %*c %*d %*d %*d %*d %*d %*u %*u %*u %*u %*u %lu %lu", name, &user_time, &system_time) != EOF)) {
            // If the current pid no longer references the same program as before
            if ((process->pid != -1) && strcmp(name, process->name)) {
                reset_process(process);
            }
            if (process->pid == -1) {
                strcpy(process->name, name);
                process->current_user = user_time;
                process->current_system = system_time;
            }
            else {
                process->previous_user = process->current_user;
                process->previous_system = process->current_system;
                process->current_user = user_time;
                process->current_system = system_time;
                process->ratio_user = process->current_user - process->previous_user;
                process->ratio_system = process->current_system - process->previous_system;
            }
        }
        else {
            result = 1;
        }
        fclose(status);
    }
    else {
        result = 2;
    }
    return result;
}

static void computeCpuLoad(void) {
    int num;
    FILE *fileSource;
    cpuload = 0;
    fileSource = fopen(CPU_PATH, "r");
    if (!fileSource) {
        return;
    }

    long long unsigned int cpu_elem[9] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
    num = fscanf(fileSource, "cpu  %llu %llu %llu %llu %llu %llu %llu %llu %llu", &cpu_elem[0], &cpu_elem[1], &cpu_elem[2], &cpu_elem[3], &cpu_elem[4], &cpu_elem[5], &cpu_elem[6], &cpu_elem[7], &cpu_elem[8]);

    if (num != 9) {
        fclose(fileSource);
        return;
    }

    for (int i = 0; i < 9; i++) {
        cpuload = cpuload + cpu_elem[i];
    }
    fclose(fileSource);
    return;
}

// Checks the cpu usage of every process.
static struct proc **check_all_processes(void) {
    DIR *d;
    struct dirent *dir;
    d = opendir(PROC_DIR);
    if (d == NULL) {
        return NULL;
    }

    while ((dir = readdir(d)) != NULL) {
        if ((dir->d_type == DT_DIR) && is_proc(dir->d_name)) {
            char proc_dir[strlen(PROC_DIR) + strlen(dir->d_name) + 1];
            strcpy(proc_dir, PROC_DIR);
            strcat(proc_dir, dir->d_name);
            int pos = find_process_id_by_pid(processes, atoi(dir->d_name), used_size);
            if (pos == -1) {
                int result = check_process_usage(processes[used_size], proc_dir);
                if (result == 0) {
                    processes[used_size]->pid = atoi(dir->d_name);
                    if (used_size == processes_size - 1) {
                        struct proc **tmp_processes = (struct proc **) realloc(processes, sizeof(struct proc *) * processes_size * 2);
                        if (tmp_processes) {
                            processes_size *= 2;
                            used_size = used_size + 1;
                            processes = tmp_processes;
                        }
                        else {
                            SAH_TRACEZ_ERROR(ME, "Cannot re-allocate memory for processes");
                        }
                        for (int i = used_size; i < processes_size; i++) {
                            processes[i] = (struct proc *) malloc(sizeof(struct proc));
                            if (!processes[i]) {
                                SAH_TRACEZ_ERROR(ME, "Cannot allocate memory for process %d", i);
                                // set the size of the array to the last element we succeeded to allocate
                                processes_size = i;
                                break;
                            }
                            reset_process(processes[i]);
                        }
                    }
                    else {
                        used_size++;
                    }
                }
            }
            else {
                check_process_usage(processes[pos], proc_dir);
            }
        }
        else if (!strcmp(dir->d_name, "stat")) {
            computeCpuLoad();
        }
    }
    closedir(d);
    return processes;
}

/* END */


static void removeChar(char *str, char garbage) {

    char *src, *dst;
    for (src = dst = str; *src != '\0'; src++) {
        *dst = *src;
        if (*dst != garbage) {
            dst++;
        }
    }
    *dst = '\0';
}

#define TYPE_URL_CPUPP_TOP10 "type.googleapis.com/kpi.cpupp.v1.CpuppTop10"
#define TYPE_URL_CPU_GLOBAL "type.googleapis.com/kpi.cpu.v1.CpuGlobal"

/* Top10 */

void cpu_Top10(void) {
    clean_processes();

    struct proc **tmp = check_all_processes();
    if (tmp == NULL) {
        SAH_TRACEZ_ERROR(ME, "Cannot browse system process list, giving up KPI report");
        return;
    }
    processes = tmp;

    if (used_size == 0) {
        SAH_TRACEZ_INFO(ME, "used_size = 0");
        return;
    }

    if (!cpuload) {
        SAH_TRACEZ_INFO(ME, "cpuload = 0");
        return;
    }

    // if we haven't calculated the right values yet.
    if (!cpuload_prev) {
        cpuload_prev = cpuload;
        SAH_TRACEZ_INFO(ME, "First measurement");
        return;
    }
    long long unsigned int cpu_total_pp = (cpuload - cpuload_prev) / 100;

    if (!cpu_total_pp) {
        SAH_TRACEZ_INFO(ME, "Current cpu load is equal than previous");
        return;
    }
    cpuload_prev = cpuload;

    qsort(processes, used_size, sizeof(processes[0]), compare);

    if ((processes[0]->ratio_user == 0) && (processes[0]->ratio_system == 0)) {
        SAH_TRACEZ_INFO(ME, "All 0%%");
        return;
    }

    // Display top 10 cpu consumers :
    int topnb = 10;

    if (used_size < 10) {
        topnb = used_size;
    }

    // init ce_batch (of size 10 for Cpu Top10)
    Io__Cloudevents__V1__CloudEventBatch ce_batch = IO__CLOUDEVENTS__V1__CLOUD_EVENT_BATCH__INIT;
    init_ce_batch(&ce_batch, topnb);

    for (int i = 0; i < topnb; i++) {
        if (!processes[i] || !processes[i]->name) {
            continue;
        }
        int process_percentage = (processes[i]->ratio_user + processes[i]->ratio_system) / cpu_total_pp;

        char *name = strdup(processes[i]->name);
        removeChar(name, '(');
        removeChar(name, ')');

        Prplprobe__Internal__V1__Event e_cpupp_top10 = PRPLPROBE__INTERNAL__V1__EVENT__INIT;
        init_event(&e_cpupp_top10, "Cpupp", "Top10");

        // KPI to recover
        Kpi__Cpupp__V1__CpuppTop10 kpi = KPI__CPUPP__V1__CPUPP_TOP10__INIT;

        // add KPI to protobuf data type
        kpi.rank = i + 1;
        kpi.name = name;
        kpi.pid = processes[i]->pid;
        kpi.systemtimeticks = processes[i]->ratio_system;
        kpi.usertimeticks = processes[i]->ratio_user;
        kpi.percentagecpu = process_percentage;

        // Macro to set data to any type of event
        set_event_data(kpi__cpupp__v1__cpupp_top10, e_cpupp_top10, kpi, TYPE_URL_CPUPP_TOP10);

        // Add event to ce_batch
        add_event_to_batch(&ce_batch, e_cpupp_top10);

        free(name);
        del_event(&e_cpupp_top10);
    }

    // generate and send the event (base64 encoded serialized protobuf message)
    char *b64_msg = gen_message(&ce_batch);
    if (b64_msg) {
        dc_logEvent(b64_msg, strlen(b64_msg) + 1, "CPUPP");
        free(b64_msg);
    }

    // free all resources
    del_ce_batch(&ce_batch);
}

/* END */

/* Global */

void cpu_Global(void) {

    FILE *fileSource = fopen(CPU_PATH, "r");
    if (!fileSource) {
        return;
    }

    memcpy(cpuload_element_prev, cpuload_element, sizeof(cpuload_element));

    // looks at the first line
    int num = fscanf(fileSource, "cpu %llu %llu %llu %llu %llu %llu %llu %llu %llu", &cpuload_element[0], &cpuload_element[1], &cpuload_element[2], &cpuload_element[3], &cpuload_element[4], &cpuload_element[5], &cpuload_element[6], &cpuload_element[7], &cpuload_element[8]);
    fclose(fileSource);

    long long unsigned int zero_all[] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
    if (!memcmp(cpuload_element_prev, zero_all, sizeof(cpuload_element_prev))) {
        return;
    }
    if (num != 9) {
        return;
    }

    long long unsigned int cpuload_element_ratio[9] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
    size_t array_length = sizeof(cpuload_element_ratio) / sizeof(cpuload_element_ratio[0]);
    for (size_t i = 0; i < array_length; i++) {
        cpuload_element_ratio[i] = cpuload_element[i] - cpuload_element_prev[i];
    }

    long long unsigned int cputotal = 0;
    for (size_t i = 0; i < array_length; i++) {
        cputotal += cpuload_element_ratio[i];
    }
    cputotal = cputotal / 100;

    // init ce_batch (of size 10 for Cpu Top10)
    Io__Cloudevents__V1__CloudEventBatch ce_batch = IO__CLOUDEVENTS__V1__CLOUD_EVENT_BATCH__INIT;
    init_ce_batch(&ce_batch, 1);

    Prplprobe__Internal__V1__Event e_cpu_global = PRPLPROBE__INTERNAL__V1__EVENT__INIT;
    init_event(&e_cpu_global, "Cpu", "Global");

    // KPI to recover
    Kpi__Cpu__V1__CpuGlobal kpi = KPI__CPU__V1__CPU_GLOBAL__INIT;

    kpi.cpu_user = cpuload_element_ratio[0] / cputotal;
    kpi.cpu_nice = cpuload_element_ratio[1] / cputotal;
    kpi.cpu_system = cpuload_element_ratio[2] / cputotal;
    kpi.cpu_idle = cpuload_element_ratio[3] / cputotal;
    kpi.cpu_iowait = cpuload_element_ratio[4] / cputotal;
    kpi.cpu_irq = cpuload_element_ratio[5] / cputotal;
    kpi.cpu_softirq = cpuload_element_ratio[6] / cputotal;
    kpi.cpu_steal_time = cpuload_element_ratio[7] / cputotal;
    kpi.cpu_guest_system = cpuload_element_ratio[8] / cputotal;

    // Macro to set data to any type of event
    set_event_data(kpi__cpu__v1__cpu_global, e_cpu_global, kpi, TYPE_URL_CPU_GLOBAL);

    // Add event to ce_batch
    add_event_to_batch(&ce_batch, e_cpu_global);

    del_event(&e_cpu_global);

    // generate and send the event (base64 encoded serialized protobuf message)
    char *b64_msg = gen_message(&ce_batch);
    if (b64_msg) {
        dc_logEvent(b64_msg, strlen(b64_msg) + 1, "CPU");
        free(b64_msg);
    }

    // free all resources
    del_ce_batch(&ce_batch);
}

/* END */

void cpu_cleanup(void) {
    reset_all_processes();

    if (processes) {
        for (int i = 0; i < processes_size; i++) {
            if (processes[i]) {
                free(processes[i]);
            }
        }
        free(processes);
    }
}

void cpu_init(void) {
    processes = (struct proc **) malloc(sizeof(struct proc *) * PATH_DEFAULT_SIZE);

    for (int i = 0; i < PATH_DEFAULT_SIZE; i++) {
        processes[i] = (struct proc *) malloc(sizeof(struct proc));

        if (!processes[i]) {
            // set the size of the array to the last element we succeeded to allocate
            processes_size = i;
            break;
        }
        reset_process(processes[i]);
    }
}
