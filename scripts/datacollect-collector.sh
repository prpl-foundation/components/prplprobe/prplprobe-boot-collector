#!/bin/sh

[ -f /etc/environment ] && source /etc/environment

ulimit -c ${ULIMIT_CONFIGURATION:-0}

start() {
    /usr/lib/datacollect/collectors/datacollect-collector &
}

stop() {
    kill $(pidof datacollect-collector)
}

case $1 in
    start|boot)
        start
        ;;
    stop|shutdown)
        stop
        ;;
    restart)
        $0 stop
        $0 start
        ;;
    debuginfo)
        echo "TODO debuginfo datacollect-collector"
        ;;
    log)
        echo "TODO log datacollect-collector"
        ;;
    *)
        echo "Usage : $0 [start|boot|stop|shutdown|restart|debuginfo|log]"
        ;;
esac
