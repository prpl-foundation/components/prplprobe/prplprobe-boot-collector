include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C proto all
	$(MAKE) -C src all

clean:
	$(MAKE) -C proto clean
	$(MAKE) -C src clean

install: all
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/object/$(COMPONENT).so $(DEST)/usr/lib/datacollect/collectors/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(DEST)$(INITDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0644 config/datacollect-config.cfg $(DEST)/etc/config/datacollect/datacollect-config.cfg
	$(INSTALL) -d -m 0755 $(DEST)//etc/config/datacollect/certs
	$(INSTALL) -D -p -m 0644 config/certs/*.pem $(DEST)/etc/config/datacollect/certs/

package: all
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/object/$(COMPONENT).so $(PKGDIR)/usr/lib/datacollect/collectors/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(PKGDIR)$(INITDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0644 config/datacollect-config.cfg $(PKGDIR)/etc/config/datacollect/datacollect-config.cfg
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/config/datacollect/certs
	$(INSTALL) -D -p -m 0644 config/certs/*.pem $(PKGDIR)/etc/config/datacollect/certs/
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

.PHONY: all clean changelog install package