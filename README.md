# Prplprobe Boot Collector

Prplprobe is a monitoring system designed to offer key performance indicators (KPIs) about the system (CPU, Memory, Wi-Fi, etc).

## Table of Contents

[[_TOC_]]

## Introduction

`prplprobe-boot-collector` is a service used to collect KPIs about boot. For now the following events are implemented:
- UpTime

## Installation

### Prerequisites

The follwoing packages are required to build `prplprobe-boot-collector`:

- CMake

### Dependencies

`prplprobe-boot-collector` relies on the following dependencies:

- Protobuf (https://github.com/protocolbuffers/protobuf)
- gRPC (https://github.com/grpc/grpc)
- libsahtrace (https://gitlab.com/prpl-foundation/components/core/libraries/libsahtrace)
- libprplprobe (https://gitlab.com/prpl-foundation/components/prplprobe/libprplprobe)

### Build from source

1. **Clone the repository**:

```sh
git clone https://gitlab.com/prpl-foundation/components/prplprobe/prplprobe-boot-collector.git
cd prplprobe-boot-collector
```

2. **Build the service**

```sh
mkdir build
cd build
cmake ..
make
make install
```

## Documentation

### Prerequisites

The follwoing packages are required to build `prplprobe-boot-collector` documentation:

- Doxygen
- Graphviz

### Build doc

Both protobuf and C++ APIs documentations are generated in the same way:

```sh
cmake .. -DGEN_DOC=y
make
make install
```

## Configuration

### Build-time configuration

During the build process, you can customize `prplprobe-boot-collector` by setting specific configuration options to meet your project's requirements. Below are the available build configuration options:

- **`GEN_DOC`**: Enabling this option generates documentation for `prplprobe-boot-collector`. Make sure to set this to `y` to generate documentation during the build process.

- **`COVERAGE`**: If you wish to include coverage flags and run tests with coverage analysis, enable this option by setting it to `y`.

To configure these options during the build process, you can set them in cmake command directly or use your buildsystem options.

## Run-time Configuration

`prplprobe-boot-collector` is configured using a JSON-formatted protobuf message in a configuration file. Below is an example of the configuration file structure:

```json
{
    "sahtrace_config": {
        "sah_trace": {
            "type": "SYSLOG",
            "level": 200
        },
        "trace_zones": [
            {
                "name": "all",
                "level": 200
            }
        ]
    }
}
```

In this example configuration, you can see the structure for specifying sahtrace configuration. Users can modify this JSON file to customize the `prplprobe-boot-collector`'s behavior according to their requirements.

## Testing

### Prerequisites

The follwoing packages are required to build `prplprobe-boot-collector` documentation:
- gcovr
- valgrind

### Build and run tests

```sh
cmake .. -DCOVERAGE=y
make
```

## License

`prplprobe-boot-collector` is licensed under the [BSD-2-Clause-Patent](https://spdx.org/licenses/BSD-2-Clause-Patent.html) license.

You can find a copy of the license in the [LICENSE](./LICENSE) file.

Please review the license carefully before using or contributing to this project. By participating in the `prplprobe-boot-collector` community, you agree to adhere to the terms and conditions set forth in this license.

For more details about the license and its implications, refer to the [full license text](https://spdx.org/licenses/BSD-2-Clause-Patent.html).
