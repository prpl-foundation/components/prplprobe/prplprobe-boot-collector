/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS AS IS AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _CLOUDEVENT_H
#define _CLOUDEVENT_H

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/sysinfo.h>
#include <sys/time.h>
#include <ctype.h>

#include <debug/sahtrace.h>

#include <utils.h>
#include <common.pb-c.h>
#include <cloudevent.pb-c.h>

#define set_event_data(type, event, kpi_v, url) \
    do { \
        uint32_t len = type ## __get_packed_size(&kpi_v); \
        uint8_t *buf = (uint8_t *) malloc(len); \
        type ## __pack(&kpi_v, buf); \
        event.event_data->type_url = strdup(url); \
        event.event_data->value.len = len; \
        event.event_data->value.data = buf; \
    } while (0)

//void set_e_timestamp(Prplprobe__Internal__V1__Event *event);
void init_event(Prplprobe__Internal__V1__Event *event, const char *kpiname, const char *kpitype);
void del_event(Prplprobe__Internal__V1__Event *event);
//void ce_attval_set_string(Io__Cloudevents__V1__CloudEvent *ce, const char *attr_name, const char *attr_value);
//void ce_attval_set_bool(Io__Cloudevents__V1__CloudEvent *ce, const char *attr_name, bool attr_value);
//void ce_attval_set_int(Io__Cloudevents__V1__CloudEvent *ce, const char *attr_name, int attr_value);
//void ce_attval_set_timestamp(Io__Cloudevents__V1__CloudEvent *ce, const char *attr_name, Prplprobe__Internal__V1__Timestamp *attr_value);
void add_event_to_batch(Io__Cloudevents__V1__CloudEventBatch *ce_batch, Prplprobe__Internal__V1__Event event);
void init_ce_batch(Io__Cloudevents__V1__CloudEventBatch *ce_batch, size_t size);
void del_ce_batch(Io__Cloudevents__V1__CloudEventBatch *ce_batch);
char *gen_message(Io__Cloudevents__V1__CloudEventBatch *ce_batch);

#endif  /* _CLOUDEVENT_H */
